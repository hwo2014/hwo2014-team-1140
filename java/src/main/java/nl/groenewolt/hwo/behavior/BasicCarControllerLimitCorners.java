package nl.groenewolt.hwo.behavior;

import java.io.Writer;

import nl.groenewolt.hwo.messages.CarPositions;
import nl.groenewolt.hwo.messages.GameInit;
import nl.groenewolt.hwo.messages.data.CarPosition;
import nl.groenewolt.hwo.messages.data.YourCar;
import noobbot.SendMsg;
import noobbot.Throttle;

public class BasicCarControllerLimitCorners implements ICarController {

	protected String myCar;
	protected double maxThrottle = 0;
	protected double throttle = 0;
	protected int cornerLimit;

	public BasicCarControllerLimitCorners(double d, int i) {
		maxThrottle = d;
		cornerLimit = i;
	}

	@Override
	public SendMsg handleCarPositions(CarPositions positions) {
		CarPosition myPosition = positions.myPosition(myCar);
		if (Math.abs(myPosition.angle) > cornerLimit) {
    		throttle -= 0.1;
    		if (throttle < 0) {
    			throttle = 0;
    		}
    	} else {
    		throttle = maxThrottle;
    	}
		return new Throttle(throttle);
	}

	@Override
	public void setCsvLogger(Writer fw) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleGameInit(GameInit gi) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleYourCar(YourCar yc) {
		myCar = yc.data.color;
	}

}
