package nl.groenewolt.hwo.behavior;

import nl.groenewolt.hwo.messages.CarPositions;
import nl.groenewolt.hwo.messages.data.CarPosition;
import noobbot.SendMsg;
import noobbot.Throttle;

public class BetterCarControllerLimitCorners extends
		BasicCarControllerLimitCorners {

	private double previousAngle;
	
	public BetterCarControllerLimitCorners(double d, int i) {
		super(d, i);
	}

	@Override
	public SendMsg handleCarPositions(CarPositions positions) {
		CarPosition myPosition = positions.myPosition(myCar);
		if (((Math.signum(previousAngle) > 0 && myPosition.angle > previousAngle) ||
	    		(Math.signum(previousAngle) < 0 && myPosition.angle < previousAngle))
	    		&& Math.abs(myPosition.angle) > cornerLimit
	    		) {
    		throttle -= 0.1;
    		if (throttle < 0) {
    			throttle = 0;
    		}
    	} else {
    		throttle = maxThrottle;
    	}
		previousAngle = myPosition.angle;
		return new Throttle(throttle);
	}
}
