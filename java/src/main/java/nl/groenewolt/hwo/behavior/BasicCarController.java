package nl.groenewolt.hwo.behavior;

import java.io.Writer;

import nl.groenewolt.hwo.messages.CarPositions;
import nl.groenewolt.hwo.messages.GameInit;
import nl.groenewolt.hwo.messages.data.YourCar;
import noobbot.SendMsg;
import noobbot.Throttle;

/**
 * Zet altijd de throttle op de vaste waarde die meegegeven is in de constructor
 */
public class BasicCarController implements ICarController {

	private double throttle;
	private Writer fw;

	public BasicCarController(double throttle) {
		this.throttle = throttle;
	}
	
	@Override
	public SendMsg handleCarPositions(CarPositions positions) {
		return new Throttle(throttle);
	}

	@Override
	public void setCsvLogger(Writer fw) {
		this.fw = fw;
		
	}

	@Override
	public void handleGameInit(GameInit gi) {
	}

	@Override
	public void handleYourCar(YourCar yc) {
	}

}
