package nl.groenewolt.hwo.behavior;

import java.io.IOException;
import java.io.Writer;

import nl.groenewolt.hwo.messages.CarPositions;
import nl.groenewolt.hwo.messages.GameInit;
import nl.groenewolt.hwo.messages.data.CarPosition;
import nl.groenewolt.hwo.messages.data.Piece;
import nl.groenewolt.hwo.messages.data.PiecePosition;
import nl.groenewolt.hwo.messages.data.TrackData;
import nl.groenewolt.hwo.messages.data.YourCar;
import nl.groenewolt.hwo.util.LimitedQueue;
import noobbot.NextCornerInfo;
import noobbot.SendMsg;
import noobbot.Switch;
import noobbot.Throttle;

public class CarController1 implements ICarController {

	private static final double MIN_DRIVING_THROTTLE = 0.3;
	private Writer fw;
	private String myCar;
	
	private double throttle = 0;
	private double previousAngle;
	
	private LimitedQueue<CarPosition> lastPositions = new LimitedQueue<>(30);
	private TrackData track;
	private double speed = 0;
	public static double MAX_SPEED = 30;
//	private double desiredSpeed = Double.MAX_VALUE; // we want to go FAST ;-)
	private double desiredSpeed = MAX_SPEED; // we want to go FAST ;-)
	private boolean switchSent;
	private double currentRadius;


	private double cornerSpeedForRadius(double radius) {
		double s;
		if (radius <= 50) {
			s = 3;
		} else if (radius <= 100) {
			s = 5;
		} else {
			s = 8;
		};
		return s;
	}

	private boolean isCurve(Piece currentPiece) {
		return currentPiece.length == 0;
	}

	private NextCornerInfo distanceToNextCorner(PiecePosition piecePosition) {
    	int i = piecePosition.pieceIndex;
    	double distance = 0;
    	Piece p = track.pieces.get(i);
    	
    	if (p.length > 0) {
			// straight
    		distance = p.length - piecePosition.inPieceDistance;
		} else {
			// curve
			double length = Math.PI * p.radius * Math.toRadians(p.angle);
			distance = length - piecePosition.inPieceDistance;
		}
    	while (true) {
    		i = (i + 1) % track.pieces.size();
    		p = track.pieces.get(i);
    		if (p.length > 0) {
    			// straight
    			distance += p.length;
    		} else {
    			// curve
    			// double length = Math.PI * p.radius * Math.toRadians(p.angle);
    			break;
    		}
    	}
    	return new NextCornerInfo(p, distance);
	}
    
	// never below MIN_DRIVING_THROTTLE
    private void reduceThrottle() {
    	throttle -= 0.1;
		if (throttle < MIN_DRIVING_THROTTLE) {
			throttle = MIN_DRIVING_THROTTLE;
		}
	}
	
	@Override
	public SendMsg handleCarPositions(CarPositions positions) {
		
		CarPosition myPosition = positions.myPosition(myCar);
    	Piece currentPiece = track.pieces.get(myPosition.piecePosition.pieceIndex);
    	currentRadius = currentPiece.radius;
    	
    	if (lastPositions.size() > 0) {
    		
    		CarPosition prevPosition = lastPositions.getLast();
    		
    		if (prevPosition.piecePosition.pieceIndex == myPosition.piecePosition.pieceIndex) {
    			// calculate speed in units per tick
    			speed = myPosition.piecePosition.inPieceDistance - prevPosition.piecePosition.inPieceDistance;
    		}
    	}
    	
    	lastPositions.add(myPosition);
    	// calculate distance to next corner
    	NextCornerInfo nextCorner = distanceToNextCorner(myPosition.piecePosition);
    	
    	
//    	System.err.println(String.format("throttle: %f angle: %f", throttle, myPosition.angle));
    	
//		OLD: check if the car is straight on the track - this is probably useful later
    	if (((Math.signum(previousAngle) > 0 && myPosition.angle > previousAngle) ||
    		(Math.signum(previousAngle) < 0 && myPosition.angle < previousAngle))
    		&& Math.abs(myPosition.angle) > 20
    		) {
    		reduceThrottle();
    	} else {
    		
    		// normal racing operation
        	if (isCurve(currentPiece)) {
//        		System.err.println("radius: " + currentPiece(myPosition.piecePosition).radius);
        		switchSent = false;
        		desiredSpeed = cornerSpeedForRadius(currentPiece.radius);
//        		if (distanceToNextCorner > 60) {
//        			throttle = 0.6;
//        		}
        	} else {
        		if (nextCorner.distance < 120) {
        			desiredSpeed = cornerSpeedForRadius(nextCorner.piece.radius);
        		} else {
        			desiredSpeed = MAX_SPEED;
        		}
        	}
        	
        	if (desiredSpeed > speed) {
        		// full throttle
        		throttle = 1;
        	} else {
        		throttle = 0.2;
        	}
//    		increaseThrottle();
    	}
    	

//    	System.err.println(distanceToNextCorner);
    	
    	/*
    	if (inCurve(myPosition.piecePosition)) {
    		System.err.println("radius: " + currentPiece(myPosition.piecePosition).radius);
    		desiredSpeed = 7;
//    		if (distanceToNextCorner > 60) {
//    			throttle = 0.6;
//    		}
    	} else {
    		if (distanceToNextCorner < 100) {
    			desiredSpeed = 6;
    		} else {
    			desiredSpeed = Double.MAX_VALUE;
    		}
    	}
    	
    	if (desiredSpeed > speed) {
    		// full throttle
    		throttle = 1;
    	} else {
    		throttle = 0.4;
    	}
    	*/
//    	throttle = 0.8;
//    	System.err.println(speed);
    	
    	previousAngle = myPosition.angle;
    	try {
			fw.write(positions.gameTick + "," + speed + "," + desiredSpeed + "," + throttle + "," + currentRadius + "," + myPosition.angle + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	if (switchSent) {
    		return new Throttle(throttle);
    	} else {
    		switchSent = true;
    		if (nextCorner.piece.angle > 0) {
    			return new Switch(Switch.RIGHT);
    		} else {
    			return new Switch(Switch.LEFT);
    		}
    	}
	}

	@Override
	public void setCsvLogger(Writer fw) {
		this.fw = fw;
		try {
			fw.write("tick, speed, desiredSpeed, throttle, radius, angle\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void handleGameInit(GameInit gi) {
		track = gi.data.race.track;
	}

	@Override
	public void handleYourCar(YourCar yc) {
		myCar = yc.data.color;
	}

}
