package nl.groenewolt.hwo.behavior;

import java.io.Writer;

import nl.groenewolt.hwo.messages.CarPositions;
import nl.groenewolt.hwo.messages.GameInit;
import nl.groenewolt.hwo.messages.data.YourCar;
import noobbot.SendMsg;

public interface ICarController {

	SendMsg handleCarPositions(CarPositions positions);
	void setCsvLogger(Writer fw);
	void handleGameInit(GameInit gi);
	void handleYourCar(YourCar yc);
}
