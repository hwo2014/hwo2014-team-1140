package nl.groenewolt.hwo.messages;

import nl.groenewolt.hwo.messages.data.LapFinishedData;

/*
{"msgType":"lapFinished",
"data":{
  "car":{"name":"Lalala","color":"red"},
  "lapTime":{"lap":0,"ticks":998,"millis":16634},
  "raceTime":{"laps":1,"ticks":999,"millis":16650},
  "ranking":{"overall":1,"fastestLap":1}
  },
  "gameId":"878d7663-2bb5-43ad-ab99-75022188f637",
  "gameTick":999}
 */
public class LapFinished extends MessageGameTick {
	public LapFinishedData data;
}
