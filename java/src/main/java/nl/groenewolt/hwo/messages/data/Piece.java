package nl.groenewolt.hwo.messages.data;

/*
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
 */
public class Piece {
	public double length;
	private boolean _switch;
	public void setSwitch(boolean value) {
		_switch = value;
	}
	public double radius;
	public double angle;
}
