package nl.groenewolt.hwo.messages.data;

/*
{
        "startLaneIndex": 0,
        "endLaneIndex": 0
      }
 */
public class Lane {
	public int startLaneIndex;
	public int endLaneIndex;
}
