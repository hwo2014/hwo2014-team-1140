package nl.groenewolt.hwo.messages.data;


/*
{
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  }
 */
public class CarPosition {
	public CarData id;
	public double angle;
	public PiecePosition piecePosition;
}
