package nl.groenewolt.hwo.messages;

import java.util.ArrayList;

import nl.groenewolt.hwo.messages.data.CarPosition;

/*
{"msgType": "carPositions", "data": [
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}
 */
public class CarPositions extends MessageGameTick {
	public ArrayList<CarPosition> data;

	public CarPosition myPosition(String myCar) {
		for (CarPosition cp : data) {
			if (myCar.equals(cp.id.color)) {
				return cp;
			}
		}
		return null;
	}
}
