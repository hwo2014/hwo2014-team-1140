package nl.groenewolt.hwo.messages.data;

// {"lap":0,"ticks":998,"millis":16634}
public class LapTime {
	public int lap;
	public int ticks;
	public int millis;
}
