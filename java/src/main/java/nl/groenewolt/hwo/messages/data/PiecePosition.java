package nl.groenewolt.hwo.messages.data;


/*
{
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
 */
public class PiecePosition {
	public int pieceIndex;
	public float inPieceDistance;
	public Lane lane;
	public int lap;
}
