package nl.groenewolt.hwo.messages.data;


/*
"data":{
  "car":{"name":"Lalala","color":"red"},
  "lapTime":{"lap":0,"ticks":998,"millis":16634},
  "raceTime":{"laps":1,"ticks":999,"millis":16650},
  "ranking":{"overall":1,"fastestLap":1}
  },
 */
public class LapFinishedData {
	public CarData car;
	public LapTime lapTime;
	public LapTime raceTime;
	public Rankin ranking;
}
