package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import nl.groenewolt.hwo.behavior.BasicCarController;
import nl.groenewolt.hwo.behavior.ICarController;

/**
 * Voor mijn eigen testjes
 * 
 * @author simongroenewolt
 */
public class LocalMain extends Main {
	
	public LocalMain(BufferedReader reader, PrintWriter writer, ICarController controller, SendMsg join)
			throws IOException {
		super(reader, writer, controller, join);
		// TODO Auto-generated constructor stub
	}

	public static void main(String... args) throws IOException {
		String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new BasicCarController(0.5), new JoinRace(botName, botKey, Track.germany, 1));
	}
}
