package noobbot;

public class Switch extends SendMsg {

	public static String LEFT = "Left";
	public static String RIGHT = "Right";
	private String direction;
	
	public Switch(String direction) {
		this.direction = direction;
	}
	
	@Override
	protected Object msgData() {
		return direction;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}

}
