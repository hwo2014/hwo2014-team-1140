package noobbot;

public class BotId {

	public String name;
	public String key;
	
	public BotId(String name, String key) {
		this.name = name;
		this.key = key;
	}
}
