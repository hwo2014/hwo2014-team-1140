package noobbot;
/*
   Copyright 2014 Simon Groenewolt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DecimalFormat;

import nl.groenewolt.hwo.behavior.CarController1;
import nl.groenewolt.hwo.behavior.ICarController;
import nl.groenewolt.hwo.messages.CarPositions;
import nl.groenewolt.hwo.messages.Crash;
import nl.groenewolt.hwo.messages.GameInit;
import nl.groenewolt.hwo.messages.LapFinished;
import nl.groenewolt.hwo.messages.Spawn;
import nl.groenewolt.hwo.messages.data.YourCar;

import com.google.gson.Gson;

/**
 * Voor gebruik met de CI server / officiele races.
 * 
 * @author simongroenewolt
 */
public class Main {
	

	public static DecimalFormat DF = new DecimalFormat("#.0"); 

    final Gson gson = new Gson();
    private PrintWriter writer;
	
    public Main(final BufferedReader reader, final PrintWriter writer, ICarController behavior, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;
        FileWriter csvLogWriter = null;
        try {
        	
        csvLogWriter = new FileWriter(new File("log.csv"), false);
        
        behavior.setCsvLogger(csvLogWriter);
        

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//            System.err.println(line);
            if (msgFromServer.msgType.equals("carPositions")) {
            	final CarPositions positions = gson.fromJson(line, CarPositions.class);
            	SendMsg msg = behavior.handleCarPositions(positions);
            	send(msg);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("error")) {
                System.out.println("Error");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	final GameInit gi = gson.fromJson(line, GameInit.class);
            	behavior.handleGameInit(gi);
                System.out.println("Race init");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("lapFinished")) {
            	System.out.println("Lap finished");
            	final LapFinished lf = gson.fromJson(line, LapFinished.class);
            	send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("crash")) {
            	System.out.println("Crash");
            	final Crash crash = gson.fromJson(line, Crash.class);
            	send(new Ping());
            } else if (msgFromServer.msgType.equals("spawn")) {
            	System.out.println("spawn");
            	final Spawn crash = gson.fromJson(line, Spawn.class);
            	send(new Ping());
            } else if (msgFromServer.msgType.equals("yourCar")) {
            	final YourCar yc = gson.fromJson(line, YourCar.class);
            	behavior.handleYourCar(yc);
            	send(new Ping());
            } else {
                System.err.println(line);
                send(new Ping());
            }
        }
        } finally {
        	if (csvLogWriter != null) {
        		
        		csvLogWriter.close();
        	}
        }
    }

	private void send(final SendMsg msg) {
        String json = msg.toJson();
		writer.println(json);
        writer.flush();
    }
	
	/**
	 * Main method
	 * 
	 * @param args
	 * @throws IOException
	 */
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new CarController1(), new Join(botName, botKey));
//        new Main(reader, writer, new JoinRace(botName, botKey, Track.usa, 2));
    }
}