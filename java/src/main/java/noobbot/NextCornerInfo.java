package noobbot;

import nl.groenewolt.hwo.messages.data.Piece;

public class NextCornerInfo {
	
	public Piece piece;
	public double distance;

	public NextCornerInfo(Piece p, double distance) {
		this.piece = p;
		this.distance = distance;
	}

}
