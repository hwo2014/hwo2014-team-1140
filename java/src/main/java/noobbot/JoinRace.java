package noobbot;

/*
{"msgType": "joinRace", "data": {
  "botId": {
    "name": "keke",
    "key": "IVMNERKWEW"
  },
  "trackName": "germany",
  "carCount": 3
}}
 */
public class JoinRace extends SendMsg {

	public Track trackName;
	public int carCount;
	public BotId botId;

	public JoinRace(String name, String key, Track trackName, int carCount) {
		this.botId = new BotId(name, key);
		this.trackName = trackName;
		this.carCount = carCount;
	}
	
	@Override
	protected String msgType() {
		return "joinRace";
	}
}
